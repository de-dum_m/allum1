/*
** intro.c for allum1 in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Feb 13 10:42:17 2014 de-dum_m
** Last update Thu Feb 13 11:07:23 2014 de-dum_m
*/

#include "allum1.h"

void	print_intro()
{
  my_putstr(" ______________________ \n"
	    "( Welcome to my Allum1 ) \n"
	    " ---------------------- \n"
	    "   o \n"
	    "    o \n"
	    "         .--. \n "
	    "       |o_o | \n "
	    "       |:_/ | \n "
	    "      //   \\ \\ \n "
	    "     (|     | ) \n "
	    "    /'\\_   _/`\\ \n "
	    "    \\___)=(___/ \n\n");
  my_putstr(" ___________________________________________________\n"
	    "|\033[1mGoal: \033[0mget your opponent to remove the last"
	    " match.  |\n"
	    "|\033[1mRules: \033[0mEach player plays in turn. Each turn,"
	    " one   |\n"
	    "|must remove one or more matches from a single line |\n"
	    "|of his choice. The last person to play,            |\n"
	    "|(ie the player who removes the last                |\n"
	    "|match), looses.                                    |\n"
	    "|___________________________________________________|\n\n");
}
