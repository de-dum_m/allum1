/*
** deletion.c for allum1 in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Feb 13 10:11:21 2014 de-dum_m
** Last update Fri Mar 14 15:08:51 2014 de-dum_m
*/

#include "allum1.h"

void	find_next_match(t_game *game)
{
  while (game->map[game->x][game->y] && game->map[game->x][game->y] != '1'
	 && game->map[game->x][game->y] != '2')
    {
      game->y++;
      my_put("nd");
    }
  while (game->y > 0 && game->map[game->x][game->y] != '1'
	 && game->map[game->x][game->y] != '2')
    {
      game->y--;
      my_put("le");
    }
}

int	reprint_line(t_game *game)
{
  while (game->y > 0)
    {
      game->y--;
      my_put("le");
    }
  while (game->map[game->x][game->y])
    {
      if (game->map[game->x][game->y] == '1')
	my_putchar('|');
      else
	my_putchar(' ');
      game->y++;
    }
  game->mode = 0;
  find_next_match(game);
  return (SUCCESS);
}

int	remove_selected(t_game *game)
{
  int	i;
  int	deleted;

  i = 0;
  deleted = 0;
  while (game->map[game->x][i])
    {
      if (game->map[game->x][i] == '2')
	{
	  deleted++;
	  game->map[game->x][i] = '3';
	  reprint_line(game);
	  game->matches--;
	}
      i++;
    }
  if (deleted == 0)
    return (FAILURE);
  return (SUCCESS);
}
