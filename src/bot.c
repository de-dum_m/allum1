/*
** bot.c for src in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by armita_a
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue Feb 11 20:39:08 2014 de-dum_m
** Last update Fri Mar 14 15:08:21 2014 de-dum_m
*/

#include <unistd.h>
#include "allum1.h"

static int	find_matches(t_game *game)
{
  int		x;
  int		tmp;

  x = 0;
  tmp = 0;
  while (game->map && game->map[x])
    {
      if (matches_on_line(game->map[x]) > 1)
	return (x);
      else if (matches_on_line(game->map[x]) == 1)
	tmp = x;
      x++;
    }
  return (tmp);
}

static int	set_bot_cursor(t_game *game)
{
  int		destx;

  destx = find_matches(game);
  while (game->x > destx)
    {
      game->x--;
      my_bot_put("up");
    }
  while (game->x < destx)
    {
      game->x++;
      my_bot_put("do");
    }
  while (game->y > 0)
    {
      game->y--;
      my_bot_put("le");
    }
  find_next_match(game);
  return (SUCCESS);
}

static int	reset_player_cursor(t_game *game, int i)
{
  while (game->x > i)
    {
      game->x--;
      my_put("up");
    }
  while (game->x < i)
    {
      game->x++;
      my_put("do");
    }
  find_next_match(game);
  return (SUCCESS);
}

static int	bot_move(t_game *game)
{
  int		limit;
  int		to_delete;

  to_delete = matches_on_line(game->map[game->x]);
  if (to_delete > 1)
    limit = 1;
  else
    limit = 0;
  while (to_delete > limit && game->map[game->x][game->y])
    {
      if (game->map[game->x][game->y] == '1')
	{
	  game->map[game->x][game->y] = '2';
	  to_delete--;
	}
      game->y++;
      my_put("nd");
      usleep(100000);
    }
  return (SUCCESS);
}

int	bot_play(t_game *game)
{
  int	i;

  i = game->x;
  set_bot_cursor(game);
  bot_move(game);
  remove_selected(game);
  reset_player_cursor(game, i);
  return (SUCCESS);
}
