/*
** movement.c for allum1 in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Feb 13 10:07:47 2014 de-dum_m
** Last update Thu Feb 13 10:20:34 2014 de-dum_m
*/

#include "allum1.h"

int	move_me(t_game *game, char *dir)
{
  if (K_UP(dir) && game->x > 0 && game->map[game->x] && game->mode == 0)
    {
      my_put("up");
      game->x--;
    }
  else if (K_DO(dir) && game->map[game->x]
	   && game->map[game->x + 1] && game->mode == 0)
    {
      go_down(game);
      game->x++;
    }
  else if (K_LE(dir) && game->y > 0)
    {
      if (move_left(game) == FAILURE)
	my_putchar('\a');
    }
  else if (K_RI(dir) && game->map[game->x][game->y + 1])
    {
      if (move_right(game) == FAILURE)
	my_putchar('\a');
    }
  else if (dir[0] == '\n' && !dir[1])
    player_turn_end(game);
  find_next_match(game);
  return (SUCCESS);
}

int	go_down(t_game *game)
{
  int		i;

  i = 0;
  my_put("do");
  while (i < game->y)
    {
      my_put("nd");
      i++;
    }
  return (SUCCESS);
}

int	move_left(t_game *game)
{
  while (game->y > 0)
    {
      game->y--;
      my_put("le");
      if (game->map[game->x][game->y] == '1'
	  || game->map[game->x][game->y] == '2')
	return (SUCCESS);
    }
  return (FAILURE);
}

int	move_right(t_game *game)
{
  while (game->map[game->x][game->y])
    {
      game->y++;
      my_put("nd");
      if (game->map[game->x][game->y] == '1'
	  || game->map[game->x][game->y] == '2')
	return (SUCCESS);
    }
  return (FAILURE);
}
