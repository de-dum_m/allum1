/*
** termcap.c for allum1 in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Feb 13 10:09:42 2014 de-dum_m
** Last update Thu Feb 13 10:18:35 2014 de-dum_m
*/

#include <unistd.h>
#include <term.h>
#include <curses.h>
#include "allum1.h"

static int	my_put_param(int c)
{
  write(1, &c, 1);
  return (SUCCESS);
}

int	my_put(char *cap)
{
  tputs(tgetstr(cap, NULL), 1, &my_put_param);
  return (SUCCESS);
}

int	my_bot_put(char *cap)
{
  usleep(100000);
  my_put(cap);
  return (SUCCESS);
}
