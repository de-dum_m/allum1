/*
** main.c for src in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Feb 10 14:06:35 2014 de-dum_m
** Last update Thu Feb 13 10:52:41 2014 de-dum_m
*/

#include <stdlib.h>
#include <unistd.h>
#include <curses.h>
#include <term.h>
#include "allum1.h"

static int		set_rawmode()
{
  struct termios	termios_p;

  if (tcgetattr(0, &termios_p) == -1)
    return (FAILURE);
  termios_p.c_lflag &= ~ICANON;
  termios_p.c_lflag &= ~ECHO;
  termios_p.c_cc[VMIN] = 1;
  termios_p.c_cc[VTIME] = 0;
  tcsetattr(0, TCSANOW, &termios_p);
  if (tgetent(NULL, "xterm") == ERR)
    return (FAILURE);
  return (SUCCESS);
}

static int	init_game(char **res, t_game *game)
{
  res[0] = my_strcpy("000010000\0");
  res[1] = my_strcpy("000111000\0");
  res[2] = my_strcpy("001111100\0");
  res[3] = my_strcpy("011111110\0");
  res[4] = my_strcpy("111111111\0");
  res[5] = NULL;
  game->map = res;
  game->x = 4;
  game->y = 0;
  game->matches = 25;
  game->mode = 0;
  return (SUCCESS);
}

static int	print_matches(char **map)
{
  int		i;
  int		j;

  i = 0;
  while (map && map[i])
    {
      j = 0;
      while (map[i][j])
	{
	  if (map[i][j] == '0')
	    my_putchar(' ');
	  else if (map[i][j] == '1')
	    my_putchar('|');
	  j++;
	}
      my_putchar('\n');
      i++;
    }
  return (SUCCESS);
}

int			main(int ac, char **av)
{
  char			*res[5];
  t_game		game;
  struct termios	termios_p;

  (void)ac;
  (void)av;
  if (tcgetattr(0, &termios_p) == -1)
    return (FAILURE);
  if (set_rawmode() == FAILURE)
    return (FAILURE);
  init_game(res, &game);
  print_intro();
  print_matches(game.map);
  match_game(&game);
  tcsetattr(0, TCSANOW, &termios_p);
  return (SUCCESS);
}
