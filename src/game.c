/*
** game.c for allum1 in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Feb 10 14:07:05 2014 de-dum_m
** Last update Thu Feb 13 10:48:11 2014 de-dum_m
*/

#include <unistd.h>
#include "allum1.h"

int	matches_on_line(char *line)
{
  int		i;
  int		matches;

  i = 0;
  matches = 0;
  while (line && line[i])
    if (line[i++] == '1')
      matches++;
  return (matches);
}

int	player_turn_end(t_game *game)
{
  if (remove_selected(game) == FAILURE)
    return (FAILURE);
  if (!game->matches)
    {
      my_putstr("You have lost! Try harder, give it the best you got son.\n");
      return (SUCCESS);
    }
  bot_play(game);
  if (!game->matches)
    {
      my_putstr("You've won, I hope this makes you happy "
		"because it's making the bot super sad.\n");
      return (SUCCESS);
    }
  return (SUCCESS);
}

static int	do_selection(t_game *game, char *key)
{
  if (key[0] == ' ' && !key[1] && game->map[game->x][game->y] == '1')
    {
      game->map[game->x][game->y] = '2';
      my_put("mr");
      my_putstr("|");
      my_put("me");
      game->mode++;
      game->y++;
    }
  else if (key[0] == ' ' && !key[1] && game->map[game->x][game->y] == '2')
    {
      game->map[game->x][game->y] = '1';
      my_put("me");
      my_putstr("|");
      game->mode--;
      game->y++;
    }
  find_next_match(game);
  return (SUCCESS);
}

static int	lets_play(t_game *game)
{
  int		red;
  char		buf[4];

  if ((red = read(0, &buf, 4)) <= 0)
    return (SUCCESS);
  buf[red] = '\0';
  move_me(game, buf);
  do_selection(game, buf);
  return (SUCCESS);
}

int	match_game(t_game *game)
{
  my_put("up");
  while (game->matches)
    lets_play(game);
  return (SUCCESS);
}
