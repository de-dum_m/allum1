/*
** tools.c for allum1 in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Feb 10 14:06:49 2014 de-dum_m
** Last update Mon Feb 10 14:07:33 2014 de-dum_m
*/

#include <unistd.h>
#include <stdlib.h>
#include "allum1.h"

int	my_putchar(char c)
{
  write(1, &c, 1);
  return (SUCCESS);
}

int	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str && str[i])
    my_putchar(str[i++]);
  return (SUCCESS);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str && str[i])
    i++;
  return (i);
}

char	*my_strcpy(char *str)
{
  int	i;
  char	*res;

  i = 0;
  if (!(res = malloc(my_strlen(str))))
    return (NULL);
  while (str && str[i])
    {
      res[i] = str[i];
      i++;
    }
  return (res);
}
