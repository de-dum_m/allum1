/*
** allum1.h for lpl in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Mar 14 15:06:33 2014 de-dum_m
** Last update Fri Mar 14 15:06:34 2014 de-dum_m
*/

#ifndef ALLUM1_H_
# define ALLUM1_H_

# define FAILURE	-1
# define SUCCESS	1

# define K_UP(c)	(c[0] == 27 && c[1] == 91 && c[2] == 65)
# define K_DO(c)	(c[0] == 27 && c[1] == 91 && c[2] == 66)
# define K_LE(c)	(c[0] == 27 && c[1] == 91 && c[2] == 68)
# define K_RI(c)	(c[0] == 27 && c[1] == 91 && c[2] == 67)

typedef struct	s_game
{
  char		**map;
  int		matches;
  int		pre_turn_matches;
  int		mode;
  int		x;
  int		y;
}		t_game;

/* src/bot.c */
int	bot_play(t_game *game);

/* src/deletion.c */
int	remove_selected(t_game *game);
int	reprint_line(t_game *game);
void	find_next_match(t_game *game);

/* src/game.c */
int	matches_on_line(char *line);
int	match_game(t_game *game);
int	player_turn_end(t_game *game);

/* src/intro.c */
void	print_intro();

/* src/movement.c */
int	go_down(t_game *game);
int	move_left(t_game *game);
int	move_me(t_game *game, char *dir);
int	move_right(t_game *game);

/* src/termcap.c */
int	my_bot_put(char *cap);
int	my_put(char *cap);

/* src/tools.c */
char	*my_strcpy(char *str);
int	my_putchar(char c);
int	my_putstr(char *str);
int	my_strlen(char *str);

#endif /* !ALLUM1_H_ */
