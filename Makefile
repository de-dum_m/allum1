##
## Makefile for allum1 in /home/de-dum_m/code/B1-C-Prog_Elem/Allum1
## 
## Made by de-dum_m
## Login   <de-dum_m@epitech.net>
## 
## Started on  Tue Feb 11 10:46:49 2014 de-dum_m
## Last update Thu Feb 13 10:48:53 2014 de-dum_m
##

NAME	= allum1

SRC	= src/bot.c \
	src/deletion.c \
	src/game.c \
	src/intro.c \
	src/main.c \
	src/movement.c \
	src/termcap.c \
	src/tools.c


OBJ	= $(SRC:.c=.o)

CFLAGS	= -Wall -Wextra -W -pedantic -Iincludes -g

LDFLAGS	= -lncurses

all:	$(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME) $(LDFLAGS)
	@echo -e "[032mCompiled successfully[0m"

clean:
	rm -f $(OBJ)

fclean:	clean
	rm -f $(NAME)

re:	fclean all

.PHONY:	all re fclean clean
